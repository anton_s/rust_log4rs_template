use log::{debug, error, info, trace, warn};
use log4rs;

fn main() {
    log4rs::init_file("logging_config.yaml", Default::default()).unwrap();
    println!("Hello, world!");
    info!("log4rs info");
    trace!("log4rs trace");
    debug!("log4rs debug");
    warn!("log4rs warn");
    error!("log4rs error");
}
